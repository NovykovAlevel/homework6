package Exceptions;

public class IsAlifeException extends Exception {

    public IsAlifeException() {
    }

    public IsAlifeException(String message) {
        super(message);
    }

    public IsAlifeException(String message, Throwable cause) {
        super(message, cause);
    }

    public IsAlifeException(Throwable cause) {
        super(cause);
    }

    public IsAlifeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
