package Exceptions;

public class EqualHealthException extends Exception {

    public EqualHealthException() {
    }

    public EqualHealthException(String message) {
        super(message);
    }

    public EqualHealthException(String message, Throwable cause) {
        super(message, cause);
    }

    public EqualHealthException(Throwable cause) {
        super(cause);
    }

    public EqualHealthException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
