package Fighters;

public class HollyKnight extends Knight implements Heal{
    private float recovery;

    public HollyKnight(String name, float health, float damage, float armor, float shield, float recovery) {
        super(name, health, damage, armor, shield);
        this.recovery = recovery;
    }

    @Override
    public void heal() {
        if (this.isAlife()) {
            this.heal(recovery);
            System.out.println(this.getName() + " recovery himself by " + recovery + " hp");
        }
    }

    @Override
    public void heal(ArenaFighters fighter) {

    }

    @Override
    public void attack(ArenaFighters arenaFighter) {
        super.attack(arenaFighter);
        heal();
    }
}
