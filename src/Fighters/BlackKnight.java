package Fighters;

public class BlackKnight extends Knight{
    public BlackKnight(String name, float health, float damage, float armor, float shield) {
        super(name, health, damage, armor, shield);
    }

    private void recovery(float damage) {
        if (this.isAlife()) {
            float recovery =damage / 2;
            this.heal(recovery);
            System.out.println(this.getName() + " recovery " + recovery + " hp");
        }
    }

    @Override
    protected void damaged(float damageTaken) {
        super.damaged(damageTaken);
        recovery(damage);
    }
}
