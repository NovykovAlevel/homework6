package Fighters;

import Exceptions.IsAlifeException;

public class DragonRider extends ArenaFighters {
    private Dragon ridingDragon;
    private float healthDragon;

    public DragonRider(String name, float health, float damage, float armor) {
        super(name, health, damage, armor);
    }

    @Override
    public void attack(ArenaFighters opponent) {
        if (opponent instanceof Dragon) {
            try {
                attackDragon((Dragon) opponent);
            } catch (IsAlifeException e) {
                System.err.println(e.getMessage());
            } finally {opponent.setHealth(0);}
        } else {
            opponent.damaged(this.damage);
        }
    }

    private void attackDragon(Dragon dragon) throws IsAlifeException {
        this.ridingDragon = dragon;
        healthDragon = dragon.getHealth();
        if (healthDragon > 0){
            throw new IsAlifeException("The dragon lost, but did not die");
        }

    }
}
