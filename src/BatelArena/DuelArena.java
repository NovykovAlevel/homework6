package BatelArena;

import Exceptions.EqualHealthException;
import Fighters.*;
import Healers.Healer;

public class DuelArena extends BattleArena {
    private ArenaFighters member1;
    private ArenaFighters member2;

    public DuelArena(Healer healer, ArenaFighters member1, ArenaFighters member2) {
        super(healer);
        this.member1 = member1;
        this.member2 = member2;
    }

    @Override
    public void startBattle(int numberRounds) {
            fight(member1, member2, numberRounds);
    }

    @Override
    public void printWinner() {
        try {
            System.out.println("In a duel 1 on 1  win " + calculationOfWinner().getName());
        } catch (EqualHealthException e) {
            e.printStackTrace();
        }
    }
}
