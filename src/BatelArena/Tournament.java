package BatelArena;

import Exceptions.EqualHealthException;
import Fighters.*;
import Healers.Healer;


public class Tournament extends BattleArena {
    private ArenaFighters tournamentWinner;
    private ArenaFighters[] members;

    public Tournament(Healer healer, ArenaFighters member1, ArenaFighters member2, ArenaFighters member3,
                      ArenaFighters member4, ArenaFighters member5, ArenaFighters member6,
                      ArenaFighters member7, ArenaFighters member8) {
        super(healer);

        members = new ArenaFighters[]{member1, member2, member3, member4, member5,
                member6, member7, member8};
    }

    @Override
    public void startBattle(int numberRounds) {
        try {
            tournamentWinner = battleInTournament(numberRounds, members);
        } catch (EqualHealthException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void printWinner() {
        if (tournamentWinner != null) {
            System.out.println("The winner of tournament is " + tournamentWinner.getName());
        }
    }

    private ArenaFighters battleInTournament(int numberRounds, ArenaFighters[] members) throws EqualHealthException {
        int index = 0;
        if (members.length > 1) {
            ArenaFighters[] winners = new ArenaFighters[members.length / 2];
            for (int i = 1; i < members.length; i++) {
                if (i % 2 == 0) {
                    continue;
                }
                fight(members[i-1], members[i], numberRounds);
                ArenaFighters win = calculationOfWinner();
                winners[index++] = win;
                System.out.println("<<<" + members[i-1].getName() + " VS " + members[i].getName() + ">>>" +
                        " WINS " + win.getName() + "\n" );
            }
            battleInTournament(numberRounds, winners);
        }
        return members[0];
    }
}
