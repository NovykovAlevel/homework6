package BatelArena;

import Exceptions.EqualHealthException;
import Healers.Healer;
import Fighters.*;


public abstract class BattleArena {
    private Healer healer;
    private ArenaFighters member1;
    private ArenaFighters member2;

    public BattleArena(Healer healer) {
        this.healer = healer;
    }

    public abstract void startBattle(int numberRounds);

    public abstract void printWinner();

    protected void fight(ArenaFighters member1, ArenaFighters member2, int numberRounds) {
        this.member1 = member1;
        this.member2 = member2;
        for (int i = 0; i < numberRounds; i++) {
            if (isFightContinue()) {
                member1.attack(member2);
                member2.attack(member1);
                healer.heal(dropTheCoin() ? member1 : member2);
            }
        }
    }

    protected  ArenaFighters calculationOfWinner() throws EqualHealthException {
        if (member1 != null || member2 != null) {
            if (member1.isAlife() && member2.isAlife()) {
                return (member1.getHealth() > member2.getHealth()) ? member1 : member2;
            } else if (this.member1.isAlife()) {
                return member1;
            } else if (this.member2.isAlife()) {
                return member2;
            } else if (member1.getHealth() == member2.getHealth()){
                throw new EqualHealthException("DRAW! HP both members are equal");
            }
        }
        return null;
    }

    private boolean dropTheCoin() {
        int randomNum = (int)(Math.random() * 100);
        return (randomNum % 2) == 0;
    }

    public boolean isFightContinue() {
        return member1.isAlife() && member2.isAlife();
    }
}
