import BatelArena.*;
import Fighters.*;
import Healers.Healer;

public class Main {
    public static void main (String[] args){

        Dragon freddy = new Dragon("Freddy",360, 15, 0.3f, Dragon.EARTH|Dragon.FIRE);
        Dragon chucky = new Dragon("Chucky",320, 20, 0.4f, Dragon.WATER|Dragon.WIND);
        Dragon antaras = new Dragon("Antaras",410, 10, 0.5f, Dragon.EARTH|Dragon.WIND);
        Dragon valakas = new Dragon("Valakas",420, 10, 0.5f, Dragon.EARTH|Dragon.WIND);
        DragonRider jonSnow = new DragonRider("JonSnow", 120, 14, 5);
        Knight artur = new Knight("Artur", 150, 40.0f, 0.4f, 1);
        ArenaFighters hollyKnight = new HollyKnight("HollyKnight", 150, 20, 0.15f, 2, 5);
        ArenaFighters hellKnight = new BlackKnight("HellKnight",100, 40, 0.5f, 1);
        Healer bishop = new Healer("Bishop", 5);

        DuelArena duelArena = new DuelArena(bishop, artur, jonSnow);
        Tournament tournament = new Tournament(bishop, antaras, jonSnow, hollyKnight, hellKnight, freddy, chucky,
                artur, valakas);

//        duelArena.startBattle(10);
//        duelArena.printWinner();

        tournament.startBattle(10);
        tournament.printWinner();
    }
}
