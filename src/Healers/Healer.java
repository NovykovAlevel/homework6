package Healers;

import Fighters.*;

public class Healer implements Heal {
    private String name;
    private int heal;


    public Healer(String name, int heal) {
        this.heal = heal;
        this.name = name;
    }

    @Override
    public void heal() {

    }

    @Override
    public void heal(ArenaFighters fighter) {
        if (fighter.isAlife()) {
            fighter.heal((float) this.heal);
            System.out.println(this.name + " healing " + fighter.getName() + " " + this.heal);
        }
    }

}
